import React, { useCallback, useEffect } from "react";
import {
  SafeAreaView,
  View,
} from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { KeyboardAvoidingView } from "../../components";
import { Reducers } from "../../redux/types";
import { Box, Heading, Pressable, Image, Text, Stack, NativeBaseProvider,FlatList } from "native-base";
import styles from "./styles";
import { ENV } from "../../configs";

declare const global: { HermesInternal: null | {} };

const Component = () => {
  useIsFocused();
  const { detailMovieState } = useSelector(
    (state: Reducers) => ({
      detailMovieState: state.detailMovie,
    }),
    shallowEqual
  );
  const _renderItem = ({ item }: any) => (
    <Box alignSelf="center" p={1.5} borderColor={"blue.400"} marginLeft={2} borderWidth={1} borderRadius={16} _text={{
      fontSize: "md",
      fontWeight: "medium",
      letterSpacing: "lg",
      color: "blue.700"
    }}>
      {item.name}
    </Box>
  );
  return (
    <NativeBaseProvider>
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <Box height={250}>
            <Image opacity={80} source={{
              uri: ENV.image + detailMovieState.movie.backdrop_path
            }} alt="image base" resizeMode="cover" height={250} />
            <Box position="absolute" alignSelf={"center"} height={250} justifyContent={"center"}>
              <Image source={{
                uri: ENV.image + detailMovieState.movie.poster_path
              }} alt="image base" resizeMode="contain" height={200} width={110} />
            </Box>
            <Heading color="white" position="absolute" bottom={2} alignSelf="center">
              {detailMovieState.movie.title}
            </Heading >
          </Box>
          <Box p={1} flexDir={"row"} justifyContent="space-around" alignItems={"center"}>
            <Text fontSize="lg" color={"gray.400"}>{detailMovieState.movie.runtime} Minutes</Text>
            <Text fontSize="lg" color={"gray.400"}>{detailMovieState.movie.release_date}</Text>
            <Box width={10} height={10} borderRadius={40} bgColor="red.400" alignItems={"center"} justifyContent="center">
              <Text bold fontSize="xl" color={"white"}>{detailMovieState.movie.vote_average}</Text>
            </Box>
          </Box>
          <Box p={3} flexDir={"row"} alignItems={"center"}>
            <Text bold fontSize="lg">Genre</Text>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              keyboardShouldPersistTaps="handled"
              data={detailMovieState.movie.genres}
              keyExtractor={(item, index) => index.toString()}
              renderItem={_renderItem}
            />
          </Box>
          <Box p={3}>
            <Text bold fontSize="lg">Overview</Text>
            <Text fontSize="md" color={"gray.400"}>{detailMovieState.movie.overview}</Text>
          </Box>
        </View>
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default Component;