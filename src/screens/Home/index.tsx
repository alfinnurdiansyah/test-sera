import React, { useCallback, useEffect } from "react";
import {
  FlatList,
  SafeAreaView,
  View,
} from "react-native";
import { useIsFocused, useNavigation } from "@react-navigation/native";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { getListMovie,getDetailMovie } from "../../redux/actions";
import { Reducers } from "../../redux/types";
import { Box, Heading, Pressable, Image, Text, Stack, NativeBaseProvider } from "native-base";
import styles from "./styles";
import { ENV } from "../../configs";

const Component = () => {
  useIsFocused();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { homeState } = useSelector(
    (state: Reducers) => ({
      homeState: state.home,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(getListMovie(homeState.key));
  }, [dispatch]);

  const _handleGetDetailMovie = useCallback((id:any,title:any) => {
    dispatch(getDetailMovie(homeState.key,id));
    navigation.navigate("DetailMovie",{name:title})
  }, [dispatch]);

  const _renderItem = ({ item, index }: any) => (
      <Pressable onPress={() => _handleGetDetailMovie(item.id,item.title)}>
        <Box bg="white" shadow={2} rounded="lg" maxWidth="95%" alignSelf="center" marginTop={5}>
          <Image source={{
          uri: ENV.image+item.backdrop_path
        }} alt="image base" resizeMode="cover" height={150} roundedTop="md" />
          <Text bold position="absolute" color="white" top={0} m={[4, 4, 8]}>
            MOVIE
          </Text>
          <Stack space={2} p={[4, 4, 8]}>
            <Text color="gray.400">Release Date : {item.release_date}</Text>
            <Heading size={["md", "lg", "md"]} noOfLines={2}>
              {item.title}
            </Heading>
            <Text lineHeight={[14, 14, 14]} numberOfLines={3} color="gray.700">
              {item.overview}
            </Text>
          </Stack>
        </Box>
      </Pressable>
  );
  return (
    <NativeBaseProvider>
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.container}>
            <Box maxWidth="95%" alignSelf="center" marginTop={5}>
              <Heading color="gray.700">
                List Movie
              </Heading >
            </Box>
            <FlatList
               keyboardShouldPersistTaps="handled"
              data={homeState.listMovie}
              keyExtractor={(item, index) => index.toString()}
              renderItem={_renderItem}
            />
          </View>
        </View>
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default Component;