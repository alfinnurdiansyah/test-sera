const env = {
    host: "https://api.themoviedb.org/3/",
    image: "https://image.tmdb.org/t/p/w500"
  };
  
  export default env;