import axios from "axios";

import { ENV } from ".";

const host = axios.create({
  baseURL: ENV.host,
});

const api = {
  getListMovie: (key:String) => host.get("movie/popular?api_key="+key),
  getDetailMovie: (key:String,id:string) => host.get("movie/"+id+"?api_key="+key),
};

export default api;