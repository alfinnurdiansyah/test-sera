import React from "react";
import {
  TransitionPresets,
  createStackNavigator,
} from "@react-navigation/stack";

import Home from "../screens/Home";
import DetailMovie from "../screens/DetailMovie";

const { Navigator, Screen } = createStackNavigator();

const Stack = () => (
  <Navigator
    initialRouteName="Home"
    screenOptions={{ ...TransitionPresets.SlideFromRightIOS }}
  >
    <Screen name="Home" component={Home} options={{ header: () => null }} />
    <Screen
      name="DetailMovie"
      component={DetailMovie}
      options={({ route }) => ({ title: route.params.name })}
    />
  </Navigator>
);

export default Stack;