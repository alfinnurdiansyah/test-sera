import { Dispatch } from "../types";
import { API } from "../../configs";

// get movie
export const GET_DETAIL_MOVIE_PENDING = "GET_DETAIL_MOVIE_PENDING";
export const GET_DETAIL_MOVIE_SUCCESS = "GET_DETAIL_MOVIE_SUCCESS";
export const GET_DETAIL_MOVIE_ERROR = "GET_DETAIL_MOVIE_ERROR";

export const getDetailMovie = (key:any,id:any) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: GET_DETAIL_MOVIE_PENDING });
    const res = await API.getDetailMovie(key,id);
    dispatch({
      type: GET_DETAIL_MOVIE_SUCCESS,
      payload: { data: res.data },
    });
  } catch (err:any) {
    if (err.response) {
      dispatch({ type: GET_DETAIL_MOVIE_ERROR, payload: { data: err.response } });
    } else {
      dispatch({ type: GET_DETAIL_MOVIE_ERROR });
    }
  }
};