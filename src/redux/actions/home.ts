import { Dispatch } from "../types";
import { API } from "../../configs";

// get movie
export const GET_MOVIE_PENDING = "GET_MOVIE_PENDING";
export const GET_MOVIE_SUCCESS = "GET_MOVIE_SUCCESS";
export const GET_MOVIE_ERROR = "GET_MOVIE_ERROR";

export const getListMovie = (key:any) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: GET_MOVIE_PENDING });
    const res = await API.getListMovie(key);
    dispatch({
      type: GET_MOVIE_SUCCESS,
      payload: { data: res.data.results },
    });
  } catch (err:any) {
    if (err.response) {
      dispatch({ type: GET_MOVIE_ERROR, payload: { data: err.response } });
    } else {
      dispatch({ type: GET_MOVIE_ERROR });
    }
  }
};