import {
    GET_MOVIE_ERROR,
    GET_MOVIE_PENDING,
    GET_MOVIE_SUCCESS,
  } from "../actions";
  import { Action, HomeState } from "../types";
  
  const initialState: HomeState = {
    listMovie: [],
    isLoadingGetMovie: false,
    key: 'a2d9d9632364dde1ce2c8810b7058355',
  };
  
  export default (state = initialState, { type, payload }: Action) => {
    switch (type) {
      // get Movie
      case GET_MOVIE_PENDING:
        return { ...state, isLoadingGetSeason: true };
      case GET_MOVIE_SUCCESS:
        return { ...state, isLoadingGetSeason: false, listMovie: payload.data };
      case GET_MOVIE_ERROR:
        return { ...state, isLoadingGetSeason: false };
      default:
        return state;
    }
  };