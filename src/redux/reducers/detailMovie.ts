import {
    GET_DETAIL_MOVIE_ERROR,
    GET_DETAIL_MOVIE_PENDING,
    GET_DETAIL_MOVIE_SUCCESS,
  } from "../actions";
  import { Action, DetailMovieState } from "../types";
  
  const initialState: DetailMovieState = {
    movie: {},
    isLoadingGetDetailMovie: false,
  };
  
  export default (state = initialState, { type, payload }: Action) => {
    switch (type) {
      // get detail Movie
      case GET_DETAIL_MOVIE_PENDING:
        return { ...state, isLoadingGetSeason: true };
      case GET_DETAIL_MOVIE_SUCCESS:
        return { ...state, isLoadingGetSeason: false, movie: payload.data };
      case GET_DETAIL_MOVIE_ERROR:
        return { ...state, isLoadingGetSeason: false };
      default:
        return state;
    }
  };