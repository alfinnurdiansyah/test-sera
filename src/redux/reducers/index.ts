import { combineReducers } from "redux";

import home from "./home";
import detailMovie from "./detailMovie";

export default combineReducers({
  home,
  detailMovie,
});