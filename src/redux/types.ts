interface Payload {
    data?: any;
}
  
  interface Params {
    type: string;
    payload?: Payload;
  }
  
  export type Dispatch = (params: Params | Function) => void;
  export type GetState = () => Reducers;
  
  export interface Action {
    type: string;
    payload: Payload;
  }
  
  // Reducer
  export interface Reducers {
    home: HomeState;
    detailMovie: DetailMovieState;
  }
  
  export interface HomeState {
    listMovie: any[];
    isLoadingGetMovie: boolean;
    key: string;
  }
  export interface DetailMovieState {
    movie: object;
    isLoadingGetDetailMovie: boolean;
  }